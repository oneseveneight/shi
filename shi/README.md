#shi
**SH**ell **I**mageboard

no php, no js, just POSIX shell, gnu coreutils, nc, html and css

### features
- posting
- replies
- images
- timestamps
- more

### to use it
- `git clone <this repo>`
- `cd <this repos folder>`
- `touch log.txt`
- `cp templates/mainindex.html ./index.html`
- point your web server to ./index.html
- to run shi
	- `./shi.sh &`
	- `tail -f log.txt`
- to make a board:
	- `mkdir -p boards/<boardname>`
	- `cp templates/index.html.template boards/<boardname>/index.html`
	- `cp templates/main.css.template boards/<boardname>/main.css`
- to stop shi
	- `killall shi.sh`
- to post to shi
	- copy the script at the top of the board into your terminal
	- hit enter, follow the prompts


i know the file permissions are weird i'll fix them at some point
