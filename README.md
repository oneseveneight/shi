# shi
**SH**ell **I**mageboard
(second edition)

actually pretty decent

### features
- its all POSIX sh
	- seriously
- posting
- replies
- images
- timestamps
- more

### how to set up
- clone this repo
- cd to this repo
- `. ./console.sh` to start the shi console
	- `start_listener` to start listening for posts
- you might want to do `tail -f log.txt` in another terminal to see whats going on

### how backends work
- shi has a builtin framework for swappable backends both for post receiving and storage,
currently there is:
	- tar backend
		- like the old version of shi
		- receives tar files through nc and generates posts
		- bad
		- don't use it
	- http backend
		- listens for an http POST request with form data
		- allows user-friendly html post form
		- cool
		- good
	- file db backend
		- stores posts as a series of text files in folders
		- unchanged from old shi
		- ok
- **how backends work**
	- **listening**
		- a backend is a shell script with a `listen` function
		- this function is expected to continuously output post data it receives
		- as long as the backend has a listen function that continuously outputs data to stdout in the proper format, 
		shi doesn't care where it comes from.
	- **db**
		- a db backend is simply a shell script providing the following functions:
			- `path_to_post <post id>`
			- `create post <board> <parent> <user> <title> <content> <image_name> <image_content> <post_id> <post_type>`	
			- `get_post_data <post_data_path> <item>`
			- note that since these functions are both in the same file, `post_data_path` can be in any format you desire
			- again, shi doesn't care how this is done, only that it follows this format
			
### anatomy of a post
- user
	- name of user who posted the post
- title
	- title of post
	- this will be ignored for replies
- content
	- the text content of a post
- thumb_path
	- optional, filesystem path to the thumbnail of the post's image
- image_path 
	- optional, filesystem path to the image
- html_path
	- path to where the html of the post is stored
- post_id
	- unique numerical id of the post
- post_type
	- type of post, specifies what template to use
	- currently there is 'oppost' and 'reply' supported
- replies
	- newline separated list of post_id's for all the replies to the op post
	- a trailing /. is added to prevent `find` from seeing `post_data_path` itself

### defining new post templates
- edit your template of choice in the 'templates' folder, 
- {{{ TRIPLE BRACES }}} will be replaced with their respective variables

### style notes
- if it can be done in a single line, you can set a variable to it,
otherwise declare the variable as empty and then set it to your operation
- follow shellcheck

### files n folders
- boards
	- where all the html for the boards is stored
- console.sh
	- source this to allow you to control shi
- globals.sh
	- global variables (really just settings)
- inbox
	- where posts data is cached before being processed
- latest
	- the id of the latest post
	- very important!!
- LICENSE
	- free software, free society
- log.txt
	- log
- problems.txt
	- current issues with shi
- README.md
	- this file
- reset.sh
	- debug script that nukes all boards and completely resets shi
	- don't use this unless you've really screwed up
- shi.sh
	- memories
- templates
	- templates
- util.sh
	- utility functions employed by console.sh




this whole rewrite started because of a feature request by desvox. Unfortunately, due to the complexity of responding to http requests with sh, this feature was not added.

Sorry desvox :(
