#!/bin/sh
# DEBUG PURPOSES ONLY
# USERS SHOULD USE THE FUNCTION PROVIDED IN util.sh!

echo '0' > latest
echo "" > log.txt
rm -rf ./inbox/*
for board in boards/*
do
	echo reseting board "$board"
	cp "$board"/index.html.template "$board"/index.html
	for post in $board/*
	do
		if echo "$post" | grep -E "[1-9].*$"
		then
			echo deleting post "$post"
			rm -rf "$post"
		fi

	done
done
pkill -f nc
