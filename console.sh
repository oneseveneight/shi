#!/bin/sh
. ./globals.sh
. ./util.sh
. ./shi2.sh

alias log="tail -f $log_file"

echo "Your shell is now a shi console!"
oldps1="$PS1"
PS1="shi>"
listener_pid=""


start_shi() {
	start_listener &
	listener_pid="$!"
	echo "Started shi"
}

stop_shi() {
	kill -- -"$listener_pid"
#	kill -- -$(ps -o pgid= $listener_pid | grep -o [0-9]*)
	echo "Stopped shi"
}

restart(){
	stop_shi
	. ./globals.sh
	. ./util.sh
	. ./shi2.sh
	echo "Reloaded source files"
	start_shi
}

#tail -f "$log_file" & # uncomment this if you want constant log output to console


cleanup() {
	PS1="$oldps1"
	alias log="log"
}

trap cleanup EXIT INT TERM
