#!/bin/sh
# the file/folder db backend
# since the backend provides both the add_post and fetch_post
# functions the data path can be in whatever format you like
# TODO: Post deletion, both on front and backend

# NOTE: A post db path should always begin at $board_dir

. ./globals.sh


path_to_post() {
        post_id="$1"
        find "$board_dir" -maxdepth 4 -name "$post_id" -not -path '*/\.*'
}

create_post() {
        # This function takes strings correspoding to post
        # data and stores them in the boards folder in the
        # shi folder/file format


        local board="$1"
        local parent="$2"
        local user="$3"
        local title="$4"
        local content="$(echo "$5" | tr '\a' '\n')" # change the BEL characters back to newlines
        local image_name="$6"
        local image_content="$7"
        local post_id="$8"
        local post_type="$9"

	echo "Adding data for post with id $post_id" >> "$log_file"

        local post_directory="$board_dir/$board/$parent/$post_id" # where the post data will be stored
	post_directory="$(realpath "$post_directory")"

        echo "[file_db_backend.sh] board: $board" >> "$log_file"
        echo "[file_db_backend.sh] parent: $parent" >> "$log_file"
        echo "[file_db_backend.sh] user: $user" >> "$log_file"
        echo "[file_db_backend.sh] title: $title" >> "$log_file"
        echo "[file_db_backend.sh] content: $content" >> "$log_file"
        echo "[file_db_backend.sh] image_name: $image_name" >> "$log_file"
	# uncomment this if you want base64 spam vvv
#       echo "image_content: $image_content" >> "$log_file"         
#        echo "image_content: $image_content" | head -c 50 >> "$log_file"         
	echo "[file_db_backend.sh] post id: $post_id" >> "$log_file"
        echo "[file_db_backend.sh] post type: $post_type" >> "$log_file"

        if [ -z "$user" ]; then echo "[file_db_backend.sh] ERROR: no user" >> "$log_file"; return 1; fi
        if [ -z "$content" ]; then echo "[file_db_backend.sh] ERROR: no content"; return 1; fi
        if ! (mkdir -p "$post_directory")
        then
                echo "[file_db_backend.sh] ERROR: could not create post dir" >> "$log_file"
                return 1
        fi

	local image_path="$post_directory/$image_name"

        echo "$user" > "$post_directory/user"
        echo "$title" > "$post_directory/title"
        echo "$content" > "$post_directory/content"
        echo "$post_type" > "$post_directory/type"
        if [ -n "$image_name" ] && [ -n "$image_content" ]
        then
                echo "$image_content" | base64 -d  > "$image_path"
		echo "$image_name" > "$post_directory/image_name"
        fi

	if command -v convert > /dev/null && [ -n "$image_name" ]
	then
		local thumb_path="$post_directory/thumb_$image_name.jpg"
		echo "[file_db_backend.sh] 'convert' found, generating thumbnail" >> "$log_file"
		# convert the image to a jpeg
		if echo "$image_path" | grep '.gif'
		then
			convert "$image_path"'[0]' "$thumb_path"
		else
			convert "$image_path" "$thumb_path"
			echo "[file_db_backend.sh] converted image to jpeg" >> "$log_file"
		fi
		# re-compress the jpeg to be under 100kb
		convert "$thumb_path" -define jpeg:extent=100kb "$thumb_path"
		echo "[file_db_backend.sh] re compressed image at $thumb_path" >> "$log_file"
	fi

        echo "[file_db_backend.sh] Finished adding post $post_id on $(date)" >> "$log_file"
        echo "[file_db_backend.sh] post dir: $post_directory" >> "$log_file"

	echo "$post_directory"

        return 0
}

get_post_data() {
	# this function is given a post id and a data item to be 
	# retrieved. It returns the value of the requested item

	# TODO: file path caching

	local post_data_path="$1"
	local item="$2"
	local image_path=""

	echo "[file_db_backend.sh] Got request for $item from post at $post_data_path" >> "$log_file"

	if ! [ -d "$post_data_path" ]; then return 1; fi

	case "$item" in "parent")
		basename $(realpath "$post_data_path/..")
		;;
	"user")
		cat "$post_data_path/user"
		;;
	"title")
		cat "$post_data_path/title"
		;;
	"content")
		cat "$post_data_path/content"
		;;
	"thumb_path") # where the post's image is stored on the filesystem
		echo "thumb_$(cat "$post_data_path/$image_name").jpg"
		;;
	"image_path") # where the post's image is stored on the filesystem
		image_path="$post_data_path/$(cat "$post_data_path/image_name")"
		if [ -f "$image_path" ]
		then
			echo "$image_path"
		else
			echo ""
		fi
		;;
	"html_path") # where the post's html is on the filesystem
		echo "$post_data_path/index.html"
		;;
	"post_id")
		basename "$post_data_path"
		;;
	"post_type")
		cat "$post_data_path/type"
		;;
	"replies")
		# Newline separated list of post_id for all replies to the 
		# specified post
		# A trailing /. is added to prevent find from seeing 
		# post_data_path itself

		find "$post_data_path" \
			-mindepth 1 \
			-type d \
			-iname '[0-9]*' \
			-printf '%p\n' | sort	
		;;
	esac
}

# no paths, only use the post id
