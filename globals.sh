port="7070"
log_file="./log.txt"
board_dir="./boards"
template_dir="./templates"
latest_file="./latest"
inbox="./inbox"
image_types="(\.png|\.jpg|\.gif)"
shi_sub_url="/shi" # the sub url shi is being run from (eg. http://domain.tld/[shi/])
