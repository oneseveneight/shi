#!/bin/sh
# Tar listening backend.
# This backend looks for tarfiles in the folder specified by $inbox.
# It then un-tars them and and gets the username, title and post content
# from their respecively named files. It will use the first image matching
# one of $image_types types as the image for the post.

# TODO: Trap the exit signal to kill nc
# or maybe use telnet

# TODO: make this file backend agnostic

. ./globals.sh

echo "Using tar backend." >> "$log_file"
gen_post_from_dir() {
	local dir="$1"
	local post_to="$(basename "$dir" | sed 's/-.*//g')" # where the post wants to go

	local board=""
	local parent=""
	local user=""
	local title=""
	local content=""
	local image_name=""
	local image_content=""

	local image_path=""

	
	echo "New post"
	if echo "$post_to" | grep -qE "[0-9]$" # if our post is a reply
        then
		# set the parent to the post it is replying to
                parent="$(find $board_dir -maxdepth 3 -name "$post_to" -not -path '*/\.*')"
		# make sure the parent post exists
		if [ -z "$parent" ]; then echo "Parent post does not exist" && return 1; fi

		# and set the board value to the board it is to be posted to
                board="$(basename `dirname "$parent"`)"
		# then change the parent value from a directory path to a plain string
		parent="$(basename `realpath "$parent"`)"
        else # otherwise, the post is an OP post.
		# set the board value to the board it is to be posted to
                board="$post_to"
        fi

	image_path="$(find "$dir" | grep -Em1 "$image_types")"
	if ! [ -z "$image_path" ]
	then
		image_name="$(basename "$image_path")"
		image_content="$(cat "$image_path" | base64 -w 0)"
	fi

	user="$(cat "$dir/user")"
	content="$(tr '\n' '\a' < "$dir/content")" # change newline characters to BEL
	title="$(cat "$dir/title")"

	echo "board:$board"
	echo "parent:$parent"
	echo "user:$user"
	echo "title:$title"
	echo "content:$content"
	echo "image_name:$image_name"
	echo "image_content:$image_content"
	echo "End new post"
	
	return 0
}

listen() {
	# NOTE: Tar extraction takes a nontrivial amount of time, which
	# means if two connections were made in close succecion
	# there is a possibility that the second connection would
	# not receive the "busy" signal, and send it's data anyways
	#
	# additionally, there should be a way to prevent the sending of
	# malicious data to interfere with normal post processing
	#
	# to prevent zip bombs, break the tar -x -C operation up and use 
	# `zcat $file | head -c $file_limit`

	blocker_pid="" # the pid of the loop that sends the busy signal
	while nc -lp "$port" | tar -x -C "$inbox"
	do
		(while true
		do 
			echo "Busy" | nc -lp 7070
		done)>/dev/null & blocker_pid="$!"

		for dir in $inbox/*
		do
			echo "Got post" >> "$log_file"
			gen_post_from_dir "$dir"
			rm -rf "$dir"
		done
		kill "$blocker_pid" >/dev/null
	done
}
