#!/bin/sh
# These are the functions that are run by the admin to manage posts

file_path_to_post() {
	post_id="$1"
	find "$board_dir" -maxdepth 4 -name "$post_id" -not -path '*/\.*'
}

delete_post() {
	post_id="$1"
	rm -rf "$(path_to_post "$post_id")"
}

regen_board() {
	. ./shi2.sh
	board="$1"
	posts="$(find "$board" \
		-mindepth 1 \
		-maxdepth 1 \
		-type d \
		-iname '[0-9]*' \
		-printf '%p\n' | sort)"
	for post in $posts
	do
		echo "Generating post $post"
		insert_post "$post" "$board/index.html"
	done

}
